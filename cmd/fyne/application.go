package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"gitlab.com/high-creek-software/thequiz/assets"
	"gitlab.com/high-creek-software/thequiz/questions"
	"log"
)

type Application struct {
	app           fyne.App
	mainWindow    fyne.Window
	controlWindow fyne.Window
	control       *Control

	qp *questions.QuestionPool

	setup *Setup
	game  *Game

	aName string
	bName string
}

func New() *Application {
	//os.Setenv("FYNE_SCALE", "2")
	//os.Setenv("FYNE_THEME", "light")
	a := &Application{}
	a.app = app.New()
	//a.app.Settings().SetTheme(&myTheme{})
	a.app.SetIcon(assets.GetIconFyne())
	a.mainWindow = a.app.NewWindow("The Quiz")
	a.mainWindow.SetMaster()
	a.mainWindow.Resize(fyne.NewSize(1200, 700))

	a.setupBody()

	return a
}

func (a *Application) parseQuestions(setName string) {
	qp, err := questions.ParseQuestions(setName)
	if err != nil {
		log.Fatalln(err)
	}
	a.qp = qp
}

func (a *Application) setupBody() {
	a.setup = NewSetup(a.startCallback)
	a.mainWindow.SetContent(a.setup.Container)
}

func (a *Application) startCallback(teamAName, teamBName, setName string) {
	a.parseQuestions(setName)
	a.game = NewGame(teamAName, teamBName, a.qp.BuildGameSet(), a.questionSelected, a.backRequest, a.openControlWindow)
	a.mainWindow.SetContent(a.game.Container)
	a.aName = teamAName
	a.bName = teamBName

	a.openControlWindow()
}

func (a *Application) openControlWindow() {
	a.controlWindow = a.app.NewWindow("Control")
	a.control = NewControl(a.aName, a.bName, a)
	a.controlWindow.SetContent(a.control.Container)
	a.controlWindow.Show()
}

func (a *Application) backRequest() {
	a.mainWindow.SetContent(a.setup.Container)
	a.game = nil
	a.controlWindow.Hide()
}

func (a *Application) questionSelected(q *questions.Question) {
	log.Println("Question:", q.Question)
	a.control.SetQuestion(q)
}

func (a *Application) Start() {
	a.mainWindow.CenterOnScreen()
	a.mainWindow.Show()

	a.app.Run()
}

func (a *Application) AWins(q *questions.Question) {
	a.game.AWins(q)
}

func (a *Application) BWins(q *questions.Question) {
	a.game.BWins(q)
}

func (a *Application) NoWin(q *questions.Question) {
	a.game.NoWin(q)
}

func (a *Application) StartTimer(q *questions.Question) {
	a.game.StartTicker()
}

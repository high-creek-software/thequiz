package questions

import (
	"encoding/json"
)

func ParseQuestions(setName string) (*QuestionPool, error) {
	data := Sets[setName]
	qp := &QuestionPool{}

	err := json.Unmarshal([]byte(data), qp)
	return qp, err
}

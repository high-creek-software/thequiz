package main

import (
	_ "embed"
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/high-creek-software/thequiz/assets"
	"gitlab.com/high-creek-software/thequiz/questions"
	"golang.org/x/exp/maps"
	"golang.org/x/exp/slices"
	"time"
)

type QuestionSelected func(q *questions.Question)
type BackRequest func()

type OpenControlWindow func()

type Game struct {
	*fyne.Container

	teamAHud *TeamHub
	teamBHud *TeamHub

	progress   *widget.ProgressBar
	questionRT *widget.RichText
	answerRT   *widget.RichText

	currentSeconds float64
	done           chan bool
	ticker         *time.Ticker

	imageOneHundred      *fyne.StaticResource
	imageOneHundredSlash *fyne.StaticResource

	imageTwoHundred      *fyne.StaticResource
	imageTwoHundredSlash *fyne.StaticResource

	imageThreeHundred      *fyne.StaticResource
	imageThreeHundredSlash *fyne.StaticResource

	imageFourHundred      *fyne.StaticResource
	imageFourHundredSlash *fyne.StaticResource

	imageFiveHundred      *fyne.StaticResource
	imageFiveHundredSlash *fyne.StaticResource
}

func NewGame(teamAName, teamBName string, gameSet map[string][]*questions.Question, qs QuestionSelected, br BackRequest, ocw OpenControlWindow) *Game {
	game := &Game{}
	game.teamAHud = NewTeamHub(teamAName)
	game.teamBHud = NewTeamHub(teamBName)

	game.imageOneHundred = assets.GetAssetFyne(assets.OneHundred)
	game.imageOneHundredSlash = assets.GetAssetFyne(assets.OneHundredSlash)
	game.imageTwoHundred = assets.GetAssetFyne(assets.TwoHundred)
	game.imageTwoHundredSlash = assets.GetAssetFyne(assets.TwoHundredSlash)
	game.imageThreeHundred = assets.GetAssetFyne(assets.ThreeHundred)
	game.imageThreeHundredSlash = assets.GetAssetFyne(assets.ThreeHundredSlash)
	game.imageFourHundred = assets.GetAssetFyne(assets.FourHundred)
	game.imageFourHundredSlash = assets.GetAssetFyne(assets.FourHundredSlash)
	game.imageFiveHundred = assets.GetAssetFyne(assets.FiveHundred)
	game.imageFiveHundredSlash = assets.GetAssetFyne(assets.FiveHundredSlash)

	keys := maps.Keys(gameSet)
	slices.Sort(keys)

	vBox := container.NewVBox()
	grid := container.NewGridWithColumns(len(keys))
	vBox.Add(grid)

	vBox.Add(layout.NewSpacer())
	game.progress = widget.NewProgressBar()
	game.progress.Min = 0
	game.progress.Max = 60
	game.progress.SetValue(60)
	game.progress.TextFormatter = game.formatRemaining
	vBox.Add(game.progress)

	vBox.Add(layout.NewSpacer())
	game.questionRT = widget.NewRichTextFromMarkdown("")
	game.questionRT.Wrapping = fyne.TextWrapWord
	vBox.Add(game.questionRT)

	game.answerRT = widget.NewRichTextWithText("")
	game.answerRT.Wrapping = fyne.TextWrapWord
	vBox.Add(widget.NewSeparator())
	vBox.Add(game.answerRT)
	vBox.Add(layout.NewSpacer())

	vBox.Add(layout.NewSpacer())

	for _, key := range keys {
		rt := widget.NewRichTextFromMarkdown(fmt.Sprintf("# %s", key))
		lblBox := container.NewHBox(layout.NewSpacer(), rt, layout.NewSpacer())
		grid.Add(container.NewVBox(lblBox, widget.NewSeparator()))
	}

	for row := 0; row < 5; row++ {
		for _, key := range keys {
			cat := gameSet[key]
			q := cat[row]
			func(question *questions.Question) {
				var img *TapIcon
				callback := func() {
					img.callback = nil
					img.ShowAlt()
					game.questionRT.ParseMarkdown(fmt.Sprintf("# %s", question.Question))
					game.answerRT.ParseMarkdown("")
					qs(question)
					game.StartTicker()
				}
				switch row {
				case 0:
					img = NewTapIcon(game.imageOneHundred, game.imageOneHundredSlash, callback)
				case 1:
					img = NewTapIcon(game.imageTwoHundred, game.imageTwoHundredSlash, callback)
				case 2:
					img = NewTapIcon(game.imageThreeHundred, game.imageThreeHundredSlash, callback)
				case 3:
					img = NewTapIcon(game.imageFourHundred, game.imageFourHundredSlash, callback)
				case 4:
					img = NewTapIcon(game.imageFiveHundred, game.imageFiveHundredSlash, callback)
				}

				grid.Add(img)
			}(q)
		}
	}

	body := container.NewHBox(
		layout.NewSpacer(),
		game.teamAHud.Container,
		layout.NewSpacer(),
		vBox,
		layout.NewSpacer(),
		game.teamBHud.Container,
		layout.NewSpacer(),
	)

	backAction := widget.NewToolbarAction(theme.NavigateBackIcon(), br)
    reopen := widget.NewToolbarAction(theme.FolderNewIcon(), ocw)
	toolbar := widget.NewToolbar(backAction, widget.NewToolbarSeparator(), reopen)

	game.Container = container.NewBorder(toolbar, nil, nil, nil, body)

	return game
}

func (g *Game) StartTicker() {
	if g.ticker != nil {
		g.ticker.Stop()
		g.ticker = nil
	}
	g.currentSeconds = 60
	g.ticker = time.NewTicker(1 * time.Second)
	g.done = make(chan bool)

	go func() {
		for {
			select {
			case <-g.done:
				return
			case <-g.ticker.C:
				g.currentSeconds -= 1
				if g.currentSeconds < 0 {
					g.done <- true
					return
				}
				g.progress.SetValue(g.currentSeconds)
			}
		}
	}()
}

func (g *Game) formatRemaining() string {
	return fmt.Sprintf("%.fs", g.currentSeconds)
}

func (g *Game) AWins(q *questions.Question) {
	g.answerRT.ParseMarkdown(fmt.Sprintf("# %s", q.Answer))
	g.teamAHud.Increment(q.Difficulty)
	g.done <- true
}

func (g *Game) BWins(q *questions.Question) {
	g.answerRT.ParseMarkdown(fmt.Sprintf("# %s", q.Answer))
	g.teamBHud.Increment(q.Difficulty)
	g.done <- true
}

func (g *Game) NoWin(q *questions.Question) {
	g.answerRT.ParseMarkdown(fmt.Sprintf("# %s", q.Answer))
	g.done <- true
}

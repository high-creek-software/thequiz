package questions

import (
	"math/rand"
	"time"
)

type QuestionPool struct {
	Categories map[string]*Category
}

type Category struct {
	Name   string
	Ones   []*Question
	Twos   []*Question
	Threes []*Question
	Fours  []*Question
	Fives  []*Question
}

func (qp *QuestionPool) addQuestion(q Question) {
	if _, ok := qp.Categories[q.Category]; !ok {
		qp.Categories[q.Category] = &Category{Name: q.Category}
	}

	cat := qp.Categories[q.Category]
	switch q.Difficulty {
	case 1:
		cat.Ones = append(cat.Ones, &q)
	case 2:
		cat.Twos = append(cat.Twos, &q)
	case 3:
		cat.Threes = append(cat.Threes, &q)
	case 4:
		cat.Fours = append(cat.Fours, &q)
	case 5:
		cat.Fives = append(cat.Fives, &q)
	}
}

func (qp *QuestionPool) BuildGameSet() map[string][]*Question {
	gs := make(map[string][]*Question)

	rand.Seed(time.Now().Unix())

	for key, val := range qp.Categories {
		gs[key] = []*Question{qp.selectRandom(val.Ones), qp.selectRandom(val.Twos),
			qp.selectRandom(val.Threes), qp.selectRandom(val.Fours), qp.selectRandom(val.Fives)}
	}

	return gs
}

func (qp *QuestionPool) selectRandom(opts []*Question) *Question {
	if opts == nil || len(opts) == 0 {
		return nil
	}
	idx := rand.Intn(len(opts))
	return opts[idx]
}

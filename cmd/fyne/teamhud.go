package main

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

type TeamHub struct {
	*fyne.Container

	nameLbl  *widget.RichText
	scoreLbl *widget.RichText

	score int
}

func NewTeamHub(name string) *TeamHub {
	th := &TeamHub{}
	th.nameLbl = widget.NewRichTextFromMarkdown(fmt.Sprintf("# %s", name))
	th.scoreLbl = widget.NewRichTextFromMarkdown("")

	vBox := container.NewVBox(th.nameLbl)
	vBox.Add(widget.NewSeparator())
	vBox.Add(th.scoreLbl)

	th.Container = vBox

	return th
}

func (th *TeamHub) Increment(difficulty int) {
	th.score += difficulty * 100
	th.scoreLbl.ParseMarkdown(fmt.Sprintf("## %d", th.score))
}

package main

import (
	"fmt"
	"github.com/diamondburned/gotk4/pkg/glib/v2"
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"github.com/diamondburned/gotk4/pkg/pango"
	"gitlab.com/high-creek-software/thequiz/questions"
	"golang.org/x/exp/maps"
	"golang.org/x/exp/slices"
	"time"
)

type QuestionCallback func(question *questions.Question)

const (
	decrement = 0.016666667
)

type Game struct {
	*gtk.Box

	aHud *TeamHud
	bHud *TeamHud

	piecesBox    *gtk.Box
	pieces       *gtk.Grid
	questionsLbl *gtk.Label
	answerLbl    *gtk.Label
	progressBar  *gtk.ProgressBar
	cb           QuestionCallback

	ticker          *time.Ticker
	done            chan bool
	currentFraction float64
}

func NewGame(teamAName, teamBName string, gameSet map[string][]*questions.Question, cb QuestionCallback) *Game {
	g := &Game{Box: gtk.NewBox(gtk.OrientationHorizontal, 0), cb: cb}
	g.SetHExpand(true)
	g.SetVExpand(true)

	g.aHud = NewTeamHud(teamAName)

	g.progressBar = gtk.NewProgressBar()
	SetAllMargin(g.progressBar, 10)

	g.piecesBox = gtk.NewBox(gtk.OrientationVertical, 0)
	g.pieces = gtk.NewGrid()
	g.pieces.SetHAlign(gtk.AlignCenter)
	g.questionsLbl = gtk.NewLabel("")
	g.questionsLbl.SetName("display-question")
	g.questionsLbl.SetWrap(true)
	g.questionsLbl.SetWrapMode(pango.WrapWord)
	SetAllMargin(g.questionsLbl, 10)
	g.questionsLbl.SetMarginTop(100)

	g.answerLbl = gtk.NewLabel("")
	g.answerLbl.SetName("display-question")
	g.answerLbl.SetWrap(true)
	g.answerLbl.SetWrapMode(pango.WrapWord)
	SetAllMargin(g.answerLbl, 10)
	g.answerLbl.SetMarginTop(100)

	g.piecesBox.Append(g.pieces)
	g.piecesBox.Append(g.progressBar)
	g.piecesBox.Append(g.questionsLbl)
	sep := gtk.NewSeparator(gtk.OrientationHorizontal)
	SetAllMargin(sep, 10)
	g.piecesBox.Append(sep)
	g.piecesBox.Append(g.answerLbl)

	g.bHud = NewTeamHud(teamBName)

	g.Append(g.aHud)
	g.Append(g.piecesBox)
	g.Append(g.bHud)

	keys := maps.Keys(gameSet)
	slices.Sort(keys)

	for idx, key := range keys {
		lbl := gtk.NewLabel(key)
		lbl.SetName("category")
		SetAllMargin(lbl, 15)
		g.pieces.Attach(lbl, idx, 0, 1, 1)
	}

	for col, key := range keys {
		for row, q := range gameSet[key] {
			btn := gtk.NewButtonWithLabel(fmt.Sprintf("%d", q.Difficulty*100))
			btn.SetName("difficulty")
			func(question *questions.Question) {
				btn.ConnectClicked(func() {
					btn.SetSensitive(false)
					cb(question)
					g.questionsLbl.SetText(question.Question)
					g.answerLbl.SetText("")
					g.progressBar.SetFraction(1)
					g.StartTicker()
				})
			}(q)
			SetAllMargin(btn, 15)
			g.pieces.Attach(btn, col, row+1, 1, 1)
			if row > 5 {
				break
			}
		}
	}

	return g
}

func (g *Game) StartTicker() {
	g.currentFraction = 1.0
	if g.ticker != nil {
		g.ticker.Stop()
		g.ticker = nil
	}
	g.ticker = time.NewTicker(1 * time.Second)
	g.done = make(chan bool)

	go func() {
		for {
			select {
			case <-g.done:
				return
			case <-g.ticker.C:
				g.currentFraction -= decrement
				glib.IdleAdd(func() {
					g.progressBar.SetFraction(g.currentFraction)
				})
				if g.currentFraction < 0.0 {
					g.done <- true
				}
			}
		}
	}()
}

func (g *Game) TeamAWin(q *questions.Question) {
	g.aHud.IncrementScore(q.Difficulty * 100)
	g.answerLbl.SetText(q.Answer)
	g.done <- true
}

func (g *Game) TeamBWin(q *questions.Question) {
	g.bHud.IncrementScore(q.Difficulty * 100)
	g.answerLbl.SetText(q.Answer)
	g.done <- true
}

func (g *Game) NoWin(q *questions.Question) {
	g.answerLbl.SetText(q.Answer)
	g.done <- true
}

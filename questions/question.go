package questions

type Question struct {
	Question   string `toml:"question"`
	Answer     string `toml:"answer"`
	Difficulty int    `toml:"difficulty"`
	Category   string `toml:"category"`
}

type questionsWrapper struct {
	Questions []Question `toml:"questions"`
}

package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
)

type TapIcon struct {
	widget.Icon

	alt      fyne.Resource
	callback func()
}

func (t *TapIcon) Tapped(_ *fyne.PointEvent) {
	if t.callback != nil {
		t.callback()
	}
}

func NewTapIcon(res, alt fyne.Resource, tapped func()) *TapIcon {
	icon := &TapIcon{callback: tapped, alt: alt}
	icon.ExtendBaseWidget(icon)
	icon.SetResource(res)

	return icon
}

func (t *TapIcon) ShowAlt() {
	t.SetResource(t.alt)
}

package main

import "github.com/diamondburned/gotk4/pkg/gtk/v4"

type StartCallback = func(teamAName, teamBName string)

type Intro struct {
	*gtk.CenterBox
	grid *gtk.Grid
}

func NewIntro(cb StartCallback) *Intro {
	i := &Intro{CenterBox: gtk.NewCenterBox(), grid: gtk.NewGrid()}

	teamAPrompt := gtk.NewLabel("Team A Name")
	SetAllMargin(teamAPrompt, 10)
	teamAEntry := gtk.NewEntry()
	SetAllMargin(teamAEntry, 10)
	teamBPrompt := gtk.NewLabel("Team B Name")
	SetAllMargin(teamBPrompt, 10)
	teamBEntry := gtk.NewEntry()
	SetAllMargin(teamBEntry, 10)

	startBtn := gtk.NewButtonWithLabel("Start")
	startBtn.ConnectClicked(func() {
		cb(teamAEntry.Text(), teamBEntry.Text())
	})
	SetAllMargin(startBtn, 10)

	i.grid.Attach(teamAPrompt, 0, 0, 1, 1)
	i.grid.Attach(teamAEntry, 1, 0, 1, 1)
	i.grid.Attach(teamBPrompt, 0, 1, 1, 1)
	i.grid.Attach(teamBEntry, 1, 1, 1, 1)
	i.grid.Attach(startBtn, 0, 2, 2, 1)

	i.SetCenterWidget(i.grid)

	return i
}

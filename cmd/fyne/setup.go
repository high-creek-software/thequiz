package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/high-creek-software/thequiz/questions"
)

type StartCallback func(teamAName, teamBName, setName string)

type Setup struct {
	*fyne.Container
}

func NewSetup(scb StartCallback) *Setup {
	teamAEntry := widget.NewEntry()
	teamAItem := widget.NewFormItem("Team A Name", teamAEntry)
	teamBEntry := widget.NewEntry()
	teamBItem := widget.NewFormItem("Team B Name", teamBEntry)
	keys := questions.GetSetNames()
	selection := widget.NewSelect(keys, nil)
	setItem := widget.NewFormItem("Question Set", selection)
	form := widget.NewForm(teamAItem, teamBItem, setItem)

	startBtn := widget.NewButton("Start", func() {
		scb(teamAEntry.Text, teamBEntry.Text, selection.Selected)
	})

	cont := container.NewGridWithRows(3,
		layout.NewSpacer(),
		container.NewGridWithColumns(3,
			layout.NewSpacer(),
			container.NewVBox(
				form,
				startBtn,
			),
			layout.NewSpacer(),
		),
		layout.NewSpacer(),
	)

	return &Setup{Container: cont}
}

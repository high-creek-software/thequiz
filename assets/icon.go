package assets

import (
	_ "embed"
	"fyne.io/fyne/v2"
)

//go:embed thequiz.png
var icon []byte

func GetIconFyne() *fyne.StaticResource {
	return &fyne.StaticResource{
		StaticName:    "thequiz.png",
		StaticContent: icon,
	}
}

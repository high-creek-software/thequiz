package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/high-creek-software/thequiz/questions"
)

type ControlCallback interface {
	AWins(q *questions.Question)
	BWins(q *questions.Question)
	NoWin(q *questions.Question)
	StartTimer(q *questions.Question)
}

type Control struct {
	*fyne.Container

	questionLbl *widget.Label
	answerLbl   *widget.Label

	teamABtn *widget.Button
	noWinBtn *widget.Button
	teamBBtn *widget.Button
	timerBtn *widget.Button

	currentQuestion *questions.Question
}

func NewControl(aName, bName string, cc ControlCallback) *Control {

	control := &Control{}

	control.questionLbl = widget.NewLabel("")
	control.questionLbl.Wrapping = fyne.TextWrapWord
	control.answerLbl = widget.NewLabel("")
	control.answerLbl.Wrapping = fyne.TextWrapWord

    control.teamABtn = widget.NewButton(aName, func() {
        if control.currentQuestion == nil {
            return
        }
		cc.AWins(control.currentQuestion)
	})

	control.noWinBtn = widget.NewButton("No Win", func() {
        if control.currentQuestion == nil {
            return
        }
		cc.NoWin(control.currentQuestion)
	})

	control.teamBBtn = widget.NewButton(bName, func() {
        if control.currentQuestion == nil {
            return
        }
		cc.BWins(control.currentQuestion)
	})

	control.timerBtn = widget.NewButton("Start Timer", func() {
        if control.currentQuestion == nil {
            return
        }
		cc.StartTimer(control.currentQuestion)
	})

	vBox := container.NewVBox(control.questionLbl,
		widget.NewSeparator(),
		control.answerLbl,
		container.NewHBox(
			control.teamABtn,
			control.noWinBtn,
			control.teamBBtn,
			control.timerBtn,
		),
	)

	border := container.NewBorder(
		layout.NewSpacer(),
		layout.NewSpacer(),
		layout.NewSpacer(),
		layout.NewSpacer(),
		vBox,
	)

	control.Container = border

	return control
}

func (c *Control) SetQuestion(q *questions.Question) {
	c.questionLbl.SetText(q.Question)
	c.answerLbl.SetText(q.Answer)

	c.currentQuestion = q
}

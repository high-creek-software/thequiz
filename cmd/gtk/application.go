package main

import (
	_ "embed"
	"github.com/diamondburned/gotk4-adwaita/pkg/adw"
	"github.com/diamondburned/gotk4/pkg/gdk/v4"
	"github.com/diamondburned/gotk4/pkg/gio/v2"
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"gitlab.com/high-creek-software/thequiz/questions"
	"log"
)

//go:embed thequiz.css
var css string

type Application struct {
	app         *adw.Application
	window      *gtk.ApplicationWindow
	cssProvider *gtk.CSSProvider

	qw *questions.QuestionPool

	intro *Intro
	stack *gtk.Stack
	game  *Game

	teamAName string
	teamBName string

	controlWindow *ControlWindow

	backButton *gtk.Button
}

func NewApplication() *Application {
	a := &Application{app: adw.NewApplication("io.highcreeksoftware.thequiz", gio.ApplicationNonUnique)}
	a.app.ConnectActivate(a.connectActivate)

	return a
}

func (a *Application) connectActivate() {
	a.window = gtk.NewApplicationWindow(&a.app.Application)
	a.window.SetDefaultSize(900, 600)
	a.cssProvider = gtk.NewCSSProvider()
	display := gdk.DisplayGetDefault()
	gtk.StyleContextAddProviderForDisplay(display, a.cssProvider, gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
	a.cssProvider.LoadFromData(css)

	qw, err := questions.ParseQuestions("christmas.toml")
	if err != nil {
		log.Fatalln(err)
	}
	a.qw = qw

	//toml.NewEncoder(os.Stdout).Encode(a.qw)

	a.setupHeader()
	a.setupBody()

	a.window.Show()
}

func (a *Application) setupHeader() {
	hb := gtk.NewHeaderBar()
	title := gtk.NewLabel("The Quiz")
	hb.SetTitleWidget(title)

	a.window.SetTitlebar(hb)

	a.backButton = gtk.NewButtonFromIconName("back")
	hb.PackStart(a.backButton)
	a.backButton.ConnectClicked(func() {
		if a.game != nil {
			a.stack.Remove(a.game)
			a.game = nil
			a.stack.SetVisibleChild(a.intro)
			if a.controlWindow != nil {
				a.controlWindow.Hide()
				a.controlWindow = nil
			}
		}
	})
}

func (a *Application) setupBody() {
	a.intro = NewIntro(a.startCallback)
	a.intro.SetVAlign(gtk.AlignCenter)

	a.stack = gtk.NewStack()
	a.stack.AddChild(a.intro)

	a.window.SetChild(a.stack)
}

func (a *Application) startCallback(teamAName, teamBName string) {
	a.teamAName = teamAName
	a.teamBName = teamBName
	a.game = NewGame(teamAName, teamBName, a.qw.BuildGameSet(), a.questionSelected)
	a.stack.AddChild(a.game)
	a.stack.SetVisibleChild(a.game)

	a.controlWindow = NewControlWindow(teamAName, teamBName, a)
	a.controlWindow.Show()
}

func (a *Application) questionSelected(q *questions.Question) {
	a.controlWindow.SetQuestion(q)
}

func (a *Application) Start() {
	a.app.Run(nil)
}

func (a *Application) TeamAWin(q *questions.Question) {
	a.game.TeamAWin(q)
}

func (a *Application) TeamBWin(q *questions.Question) {
	a.game.TeamBWin(q)
}

func (a *Application) NoWin(q *questions.Question) {
	a.game.NoWin(q)
}

func (a *Application) StartTimer() {
	a.game.StartTicker()
}

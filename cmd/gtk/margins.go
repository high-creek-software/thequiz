package main

type Marginer interface {
	SetMarginStart(int)
	SetMarginTop(int)
	SetMarginEnd(int)
	SetMarginBottom(int)
}

func SetAllMargin(m Marginer, size int) {
	m.SetMarginStart(size)
	m.SetMarginTop(size)
	m.SetMarginEnd(size)
	m.SetMarginEnd(size)
}

package main

import (
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"strconv"
)

type TeamHud struct {
	*gtk.Box
	teamLabel    *gtk.Label
	scoreLabel   *gtk.Label
	currentScore int
}

func NewTeamHud(name string) *TeamHud {
	th := &TeamHud{Box: gtk.NewBox(gtk.OrientationVertical, 0)}
	th.SetHExpand(true)
	th.SetVExpand(true)
	th.teamLabel = gtk.NewLabel(name)
	th.teamLabel.SetName("teamnames")
	SetAllMargin(th.teamLabel, 10)

	th.Append(th.teamLabel)

	sep := gtk.NewSeparator(gtk.OrientationHorizontal)
	SetAllMargin(sep, 15)
	th.Append(sep)

	th.scoreLabel = gtk.NewLabel("")
	th.scoreLabel.SetName("score")
	SetAllMargin(th.scoreLabel, 10)
	th.Append(th.scoreLabel)

	return th
}

func (th *TeamHud) IncrementScore(amount int) {
	th.currentScore += amount
	score := strconv.Itoa(th.currentScore)
	th.scoreLabel.SetText(score)
}

package main

import (
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"github.com/diamondburned/gotk4/pkg/pango"
	"gitlab.com/high-creek-software/thequiz/questions"
)

type ControlCallback interface {
	TeamAWin(q *questions.Question)
	TeamBWin(q *questions.Question)
	NoWin(q *questions.Question)
	StartTimer()
}

type ControlWindow struct {
	*gtk.Window
	vBox          *gtk.Box
	questionLabel *gtk.Label
	answerLabel   *gtk.Label

	currentQuestion *questions.Question
	controlCallback ControlCallback
}

func NewControlWindow(teamAName, teamBName string, cb ControlCallback) *ControlWindow {
	cw := &ControlWindow{Window: gtk.NewWindow(), controlCallback: cb}
	cw.SetTitle("The Quiz - Control Window")

	cw.vBox = gtk.NewBox(gtk.OrientationVertical, 0)
	cw.vBox.SetHExpand(true)
	cw.vBox.SetVExpand(true)

	cw.questionLabel = gtk.NewLabel("")
	cw.questionLabel.SetName("control-window-question")
	cw.questionLabel.SetWrap(true)
	cw.questionLabel.SetWrapMode(pango.WrapWord)
	SetAllMargin(cw.questionLabel, 25)
	cw.vBox.Append(cw.questionLabel)

	sep := gtk.NewSeparator(gtk.OrientationHorizontal)
	SetAllMargin(sep, 20)
	cw.vBox.Append(sep)

	cw.answerLabel = gtk.NewLabel("")
	cw.answerLabel.SetName("control-window-answer")
	cw.answerLabel.SetWrap(true)
	cw.answerLabel.SetWrapMode(pango.WrapWord)
	SetAllMargin(cw.answerLabel, 25)
	cw.vBox.Append(cw.answerLabel)
	cw.SetChild(cw.vBox)

	vSep := gtk.NewLabel("")
	vSep.SetVExpand(true)
	cw.vBox.Append(vSep)

	controlsBox := gtk.NewBox(gtk.OrientationHorizontal, 0)
	controlsBox.SetHAlign(gtk.AlignCenter)
	controlsBox.SetVAlign(gtk.AlignBaseline)
	controlsBox.SetMarginBottom(20)

	teamAWin := gtk.NewButtonWithLabel(teamAName)
	teamAWin.ConnectClicked(cw.handleAWin)
	SetAllMargin(teamAWin, 15)

	teamBWin := gtk.NewButtonWithLabel(teamBName)
	teamBWin.ConnectClicked(cw.handleBWin)
	SetAllMargin(teamBWin, 15)

	noWin := gtk.NewButtonWithLabel("No Win")
	noWin.ConnectClicked(cw.handleNoWin)
	SetAllMargin(noWin, 15)

	spacer := gtk.NewLabel("")
	spacer.SetHExpand(true)

	timer := gtk.NewButtonWithLabel("Start Timer")
	timer.ConnectClicked(cw.handleTimer)
	SetAllMargin(timer, 15)

	controlsBox.Append(teamAWin)
	controlsBox.Append(noWin)
	controlsBox.Append(teamBWin)
	controlsBox.Append(spacer)
	controlsBox.Append(timer)

	cw.vBox.Append(controlsBox)

	return cw
}

func (cw *ControlWindow) SetQuestion(q *questions.Question) {
	cw.currentQuestion = q
	cw.questionLabel.SetText(q.Question)
	cw.answerLabel.SetText(q.Answer)
}

func (cw *ControlWindow) handleAWin() {
	cw.controlCallback.TeamAWin(cw.currentQuestion)
}

func (cw *ControlWindow) handleBWin() {
	cw.controlCallback.TeamBWin(cw.currentQuestion)
}

func (cw *ControlWindow) handleNoWin() {
	cw.controlCallback.NoWin(cw.currentQuestion)
}

func (cw *ControlWindow) handleTimer() {
	cw.controlCallback.StartTimer()
}

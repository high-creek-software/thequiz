package assets

import (
	_ "embed"
	"fyne.io/fyne/v2"
)

//go:embed 100.png
var oneHundred []byte

//go:embed 100-slash.png
var oneHundredSlash []byte

//go:embed 200.png
var twoHundred []byte

//go:embed 200-slash.png
var twoHundredSlash []byte

//go:embed 300.png
var threeHundred []byte

//go:embed 300-slash.png
var threeHundredSlash []byte

//go:embed 400.png
var fourHundred []byte

//go:embed 400-slash.png
var fourHundredSlash []byte

//go:embed 500.png
var fiveHundred []byte

//go:embed 500-slash.png
var fiveHundredSlash []byte

const (
	OneHundred        = "100.png"
	OneHundredSlash   = "100-slash.png"
	TwoHundred        = "200.png"
	TwoHundredSlash   = "200-slash.png"
	ThreeHundred      = "300.png"
	ThreeHundredSlash = "300-slash.png"
	FourHundred       = "400.png"
	FourHundredSlash  = "400-slash.png"
	FiveHundred       = "500.png"
	FiveHundredSlash  = "500-slash.png"
)

func GetAssetFyne(name string) *fyne.StaticResource {
	sr := &fyne.StaticResource{
		StaticName: name,
	}
	switch name {
	case OneHundred:
		sr.StaticContent = oneHundred
	case OneHundredSlash:
		sr.StaticContent = oneHundredSlash
	case TwoHundred:
		sr.StaticContent = twoHundred
	case TwoHundredSlash:
		sr.StaticContent = twoHundredSlash
	case ThreeHundred:
		sr.StaticContent = threeHundred
	case ThreeHundredSlash:
		sr.StaticContent = threeHundredSlash
	case FourHundred:
		sr.StaticContent = fourHundred
	case FourHundredSlash:
		sr.StaticContent = fourHundredSlash
	case FiveHundred:
		sr.StaticContent = fiveHundred
	case FiveHundredSlash:
		sr.StaticContent = fiveHundredSlash
	}

	return sr
}

package questions

import (
	_ "embed"
	"golang.org/x/exp/maps"
)

//go:embed christmas.json
var ChristmasSet string

//go:embed theoffice.json
var TheOfficeSet string

//go:embed world.json
var WorldSet string

var Sets = map[string]string{"Christmas": ChristmasSet, "TheOffice": TheOfficeSet, "WorldTrivia": WorldSet}

func GetSetNames() []string {
	return maps.Keys(Sets)
}
